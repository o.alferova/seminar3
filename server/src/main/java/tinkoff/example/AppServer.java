package tinkoff.example;

import tinkoff.example.entity.Person;

/**
 * Hello world!
 *
 */
public class AppServer {

    public static void main(String[] args) {
        StorageManager.write(new Person("Oxana", 24, "female"));
        StorageManager.write(new Person("Petr", 19, "male"));
        StorageManager.write(new Person("Oleg", 7, "male"));
        StorageManager.write(new Person("Olga", 88, "female"));

        Server server = new Server();
        server.start();
    }
}
