package tinkoff.example;

import tinkoff.example.entity.Person;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {

    private ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

    public void start() {
        try (Selector selector = Selector.open()) {
            try (ServerSocketChannel serverSocketChannel = configServerSocketChannel(selector)) {
                while (true) {
                    selector.select();
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iter = selectionKeys.iterator();
                    while (iter.hasNext()) {
                        SelectionKey selectionKey = iter.next();
                        if (selectionKey.isAcceptable()) {
                            configSocketChannel(serverSocketChannel, selector);
                        }
                        if (selectionKey.isReadable()) {
                            try (SocketChannel client = (SocketChannel) selectionKey.channel()) {
                                client.read(byteBuffer);
                                ClientRequest clientRequest =
                                    SerializerHelper.fromByte(byteBuffer.array(), ClientRequest.class);
                                Person person = StorageManager.read(clientRequest.name());
                                ServerResponse serverResponse = new ServerResponse(person);
                                byte[] serverResponseBytes = SerializerHelper.toByte(serverResponse);
                                client.write(ByteBuffer.wrap(serverResponseBytes));
                            }
                        }
                        iter.remove();
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public SocketChannel configSocketChannel(ServerSocketChannel serverSocketChannel, Selector selector) {
        SocketChannel client;
        try {
            client = serverSocketChannel.accept();
            client.configureBlocking(true);
            client.register(selector, SelectionKey.OP_READ);
            return client;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ServerSocketChannel configServerSocketChannel(Selector selector) {
        try {
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(777));
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            return serverSocketChannel;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
