package tinkoff.example;

import tinkoff.example.entity.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StorageManager {

    public static Person read(String name) {
        Path path = Paths.get(name);
        try {
            return SerializerHelper.fromByte(Files.readAllBytes(path), Person.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void write(Person person) {
        Path path = Paths.get(person.name());
        byte[] bytes = SerializerHelper.toByte(person);
        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
