package tinkoff.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SerializerHelper {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static byte[] toByte(Object object) {
/*        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (ObjectOutputStream dataOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
            dataOutputStream.writeObject(object);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return byteArrayOutputStream.toByteArray();*/

        try {
            return objectMapper.writeValueAsBytes(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromByte(byte[] bytes, Class myClass) {
/*        try {
            try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
                return (T) objectInputStream.readObject();
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }*/

        try {
           return (T) objectMapper.readValue(bytes, myClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
