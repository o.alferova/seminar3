package tinkoff.example;

import tinkoff.example.entity.Person;

import java.io.Serializable;

public record ServerResponse(Person person) implements Serializable {

}
