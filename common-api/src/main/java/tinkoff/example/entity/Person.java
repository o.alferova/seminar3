package tinkoff.example.entity;

import java.io.Serializable;

public record Person (String name, Integer age, String sex) implements Serializable {
}
