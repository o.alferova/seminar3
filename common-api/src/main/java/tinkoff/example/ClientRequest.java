package tinkoff.example;

import java.io.Serializable;

public record ClientRequest (String name) implements Serializable {

}
