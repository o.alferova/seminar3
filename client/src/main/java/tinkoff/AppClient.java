package tinkoff;

import tinkoff.example.ClientRequest;
import tinkoff.example.SerializerHelper;
import tinkoff.example.ServerResponse;

import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class AppClient {

    static Logger logger = Logger.getAnonymousLogger();

    public static void main(String[] args) {
        Client client = new Client();
        client.start();

        ClientRequest clientRequest = new ClientRequest("Oxana");
        client.send(SerializerHelper.toByte(clientRequest));
        ServerResponse serverResponse = SerializerHelper.fromByte(client.waitResponse(), ServerResponse.class);
        logger.info(serverResponse.toString());
        client.close();
    }
}
